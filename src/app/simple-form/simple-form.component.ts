import { Component, OnInit,Input } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-simple-form',
  template: `
    <div>
      {{message}}
      <input #myInput type="text" [(ngModel)]="message">
      <button (click)=onClick(myInput.value,$event) >Click!</button>
    </div>
  `,
  styles: []
})
export class SimpleFormComponent implements OnInit {

  @Input() message;

  constructor() {
    setInterval(() =>
      this.message = Math.random().toString(),1000);
   }

  ngOnInit() {
  }

  onClick(event,value) {
    console.log(event);
    console.log(value);
  }

}
